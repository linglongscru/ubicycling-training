Feature: Calories spent are listed for each workout
  Since such information is a core part of my training
  As a user
  I want to be able to see the calories I spent on a given workout right on the list

  Background:
    Given the default features are loaded
    And I am on the section "Workouts"

  Scenario: The calories column is visible
    Then I should see text matching "Calories"
    And I should see text matching "3219 Kcal"