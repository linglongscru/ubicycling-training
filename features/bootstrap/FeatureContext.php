<?php

use Behat\Behat\Context\SnippetAcceptingContext;

class FeatureContext extends MiddlewareContext implements SnippetAcceptingContext
{
    protected $sections = [
        'Workouts' => '/workout/',
    ];

    protected function goToSection($page)
    {
        $this->visitPath($this->sections[$page]);
    }

    /**
     * @Given I am on the section :arg1
     * @Given I go to the section :arg1
     */
    public function iAmOnTheSection($section)
    {
        $this->goToSection($section);
    }

    /**
     * @Given the default features are loaded
     */
    public function defaultFixturesLoaded()
    {
        exec(__DIR__.'/../../app/console doc:fix:load -n');
    }

    /**
     * @When I click the :link link
     */
    public function iClickTheLink($link)
    {
        $this->getSession()->getPage()->clickLink($this->links[$link]);
    }
}
