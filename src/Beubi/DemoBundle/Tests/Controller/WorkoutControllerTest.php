<?php

namespace Beubi\DemoBundle\Tests\Controller;

/**
 * Dummy test
 */
class WorkoutControllerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provider()
    {
        $array = [];
        for ($index = 1; $index < 126; $index++) {
            $array[] = [$index];
        }

        return $array;
    }

    /**
     * @test
     * @dataProvider provider
     * @param int $provider
     */
    public function dummy($provider)
    {
        $this->assertInternalType('int', $provider);
    }
}
