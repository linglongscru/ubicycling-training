<?php

namespace Beubi\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Workout controller.
 *
 * @Route("/workout")
 */
class WorkoutController extends Controller
{
    /**
     * @Route("/", name="workouts")
     * @Template("BeubiDemoBundle:Default:workouts.html.twig")
     */
    public function workoutsAction()
    {
        $em = $this->getDoctrine();

        $workouts = $em->getRepository('BeubiDemoBundle:Workout')->findBy(array(), array('timestamp' => 'DESC'));

        return array(
            'workouts' => $workouts,
        );
    }

    /**
     * @Route("/redis", name="redis")
     * @Template("BeubiDemoBundle:Default:redis.html.twig")
     */
    public function redisAction()
    {
        return ['redis' => $this->container->get('snc_redis.default')->keys('*')];
    }

    /**
     * Finds and displays a Workout entity.
     *
     * @Route("/{id}", name="workout_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BeubiDemoBundle:Workout')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Workout entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }
}
